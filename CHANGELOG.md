# Changelog

## 0.1.1-SNAPSHOT (unreleased)

- Support multiple handlers in a single trigger:
  ```toml
  [[trigger]]
	[[trigger.handler]]
		name = "..."
	[[trigger.handler]]
		name = "..."
  ```

## 0.1.0 (2022-08-17)

- Initial release
