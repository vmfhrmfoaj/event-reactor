SHELL = /bin/bash -o pipefail
ifneq (,$(wildcard ./.env))
    include .env
endif

CLOJURE ?= clj

PROJ_NAME  := $(shell cat build.clj | tr -d '\n' | gawk 'match($$0,/\(def[ ]+lib[ ]+[^\/]+\/([^ ]+)\)/, m) { print m[1] }')
PROJ_VER   := $(shell cat build.clj | tr -d '\n' | gawk 'match($$0,/\(def[ ]+version[ ]+"([^ ]+)"\)/, m) { print m[1] }')

SRC_FILES      := $(shell find $(PWD)/src $(PWD)/spec -name '*.clj')
TEST_FILES     := $(shell find $(PWD)/gen $(PWD)/test -name '*.clj')
RESOURCE_FILES := $(shell find $(PWD)/resources       -name '*')
DEV_RES_FILES  := $(shell find $(PWD)/dev-resources   -name '*')
TARGET_DIR   := $(CURDIR)/target
JAR_FILE     := $(TARGET_DIR)/$(PROJ_NAME)-$(PROJ_VER).jar
UBERJAR_FILE := $(TARGET_DIR)/$(PROJ_NAME)-$(PROJ_VER)-standalone.jar
BIN_FILE     := $(TARGET_DIR)/$(PROJ_NAME)

INSTALLED_BIN_FILE := $(HOME)/.local/bin/$(PROJ_NAME)
INSTALLED_JAR_FILE := $(HOME)/.local/lib/java/$(PROJ_NAME)-$(PROJ_VER)-standalone.jar
SYSTEMD_SERVICE    := $(HOME)/.config/systemd/user/$(PROJ_NAME).service
SYSTEMD_ENV_FILE   := $(HOME)/.config/$(PROJ_NAME)/$(PROJ_NAME).sysconfig

DEPS ?=
ifdef INSIDE_CI
ifneq (,$(DEPS))
CLOJURE += -Sdeps '$(shell echo '$(DEPS)' | sed -E 's|}[ ]*$$| :mvn/local-repo ".m2/repository"}|')'
else
CLOJURE += -Sdeps '{:mvn/local-repo ".m2/repository"}'
endif
else
ifneq (,$(DEPS))
CLOJURE += -Sdeps '$(DEPS)'
endif
endif


.PHONY: help
help:  ## Display this help message
	@grep -hIE '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: run
run:  ## Run a program
	$(CLOJURE) -X $(PROJ_NAME).core/-main

.PHONY: jar
jar: $(JAR_FILE)  ## Build a jar file

.PHONY: uberjar
uberjar: $(UBERJAR_FILE)  ## Build a standalone jar file

.PHONY: bin
bin: $(BIN_FILE)  ## Build a binary file

deps:  ## Download dependencies
	$(CLOJURE) '-X:deps' prep

.PHONY: rpm
rpm:  ## Generate RPM file
	$(eval srcdir := $(shell rpm --eval '%{_sourcedir}'))
	$(eval name := $(shell rpmspec -q --queryformat '%{name}\n'    event-handler.spec | head -1))
	$(eval ver  := $(shell rpmspec -q --queryformat '%{version}\n' event-handler.spec | head -1))
	mkdir -p $(srcdir)
	rm -f $(srcdir)/$(name)-$(ver).tar.gz
	git archive --format=tar.gz --prefix=$(name)-$(ver)/ --output=$(srcdir)/$(name)-$(ver).tar.gz HEAD
	rpmbuild -ba event-handler.spec

.PHONY: isntall
install: $(INSTALLED_BIN_FILE) $(SYSTEMD_SERVICE) $(SYSTEMD_ENV_FILE)  ## Install to $HOME directory
	systemctl --user daemon-reload
	systemctl --user enable $(PROJ_NAME).service
	systemctl --user restart $(PROJ_NAME).service

.PHONY: uninstall
uninstall:  ## Uninstall from $HOME directory
	systemctl --user disable --now $(PROJ_NAME).service
	rm -f $(INSTALLED_BIN_FILE)
	rm -f $(INSTALLED_JAR_FILE)
	rm -f $(SYSTEMD_SERVICE)
	rm -f $(SYSTEMD_ENV_FILE)
	rmdir --ignore-fail-on-non-empty $(dir $(INSTALLED_JAR_FILE))
	rmdir --ignore-fail-on-non-empty $(dir $(SYSTEMD_SERVICE))
	rmdir --ignore-fail-on-non-empty $(dir $(SYSTEMD_ENV_FILE))
	systemctl --user daemon-reload

.PHONY: test
test: .test  ## Run unit tests

.PHONY: auto-test
auto-test:  ## Run unit tests whenever source files are changed
	$(CLOJURE) '-M:dev:test' --watch --plugin kaocha.plugin/notifier

.PHONY: coverage
coverage: ## Report test coverage
	$(CLOJURE) '-M:dev:test' --plugin kaocha.plugin/cloverage 2>&1 | awk '/Execution error |Instrumented [0-9]+ namespaces in / { show=1 } show { print $$0 }'

.PHONY: clean
clean:  ## Remove generated files by Clojure compiler
	rm -rf $(TARGET_DIR)
	rm -f .test
	rm -f pom.xml


pom.xml: deps.edn
	$(CLOJURE) '-X:deps' mvn-pom

.test: deps.edn $(SRC_FILES) $(TEST_FILES) $(RESOURCE_FILES) $(DEV_RES_FILES)
	$(CLOJURE) '-M:dev:test'
	@touch .test

$(JAR_FILE): deps.edn build.clj $(SRC_FILES) $(RESOURCE_FILES)
	$(CLOJURE) '-T$(EXTRA_ALIAS):build' jar
	@test -f $@ && touch $@

$(UBERJAR_FILE): deps.edn build.clj $(SRC_FILES) $(RESOURCE_FILES)
	$(CLOJURE) '-T:build' uberjar
	@test -f $@ && touch $@

$(BIN_FILE): $(UBERJAR_FILE)
ifneq (,$(STATIC_BINARY)) # see https://www.graalvm.org/latest/reference-manual/native-image/guides/build-static-executables/
	$(eval STATIC_OPTS := --static --libc=musl)
endif
	native-image \
		-O3 --gc=G1 $(STATIC_OPTS) \
		--report-unsupported-elements-at-runtime \
		--initialize-at-build-time \
		--strict-image-heap \
		'-H:+UnlockExperimentalVMOptions' \
		'-H:+BuildReport' \
		'-H:Name=$(notdir $(BIN_FILE))' '-H:Path=$(dir $(BIN_FILE))' \
		-jar $<
	@test -f $(basename $<) && mv $(basename $<) $(BIN_FILE) # '-H:Name' option doesn't work


$(INSTALLED_BIN_FILE): $(BIN_FILE)
	mkdir -p $(dir $@)
ifeq (install,$(MAKECMDGOALS))
	systemctl --user stop $(PROJ_NAME).service
endif
	cp $< $@

$(INSTALLED_JAR_FILE): $(UBERJAR_FILE)
	mkdir -p $(dir $@)
	cp $< $@

$(SYSTEMD_SERVICE): event-handler.service
	mkdir -p $(dir $@)
	cp $< $@
	sed -i 's|@BIN@|$(subst $(HOME),%h,$(INSTALLED_BIN_FILE))|g' $@
	sed -i 's|@SYSCONF@|$(subst $(HOME),%h,$(SYSTEMD_ENV_FILE))|g' $@

$(SYSTEMD_ENV_FILE): event-handler.sysconfig
	mkdir -p ~/.cache/$(PROJ_NAME)
	mkdir -p $(dir $@)
	cp $< $@
	sed -i 's|@CONFIG@|$(HOME)/.config/event-handler/config.toml|g' $@
