(ns build
  (:require [clojure.tools.build.api :as b]))

(def lib 'vmfhrmfoaj/event-handler)
(def version "0.1.1-SNAPSHOT")
(def class-dir "target/classes")
(def jar-file  (format "target/%s-%s.jar"            (name lib) version))
(def uber-file (format "target/%s-%s-standalone.jar" (name lib) version))
(def basis (delay (b/create-basis {:project "deps.edn"})))

(defn clean [_]
  (b/delete {:path "target"}))

(defn jar [_]
  (b/write-pom {:class-dir class-dir
                :lib lib
                :version version
                :basis @basis
                :src-dirs ["src"]})
  (b/copy-dir {:src-dirs ["src" "spec" "resources"]
               :target-dir class-dir})
  (b/jar {:class-dir class-dir
          :jar-file jar-file}))

(defn uberjar [_]
  (clean nil)
  (b/copy-dir {:src-dirs ["src" "spec" "resources"]
               :target-dir class-dir})
  (b/compile-clj {:basis @basis
                  :ns-compile '[event-handler.core]
                  :class-dir class-dir})
  (b/uber {:class-dir class-dir
           :uber-file uber-file
           :basis @basis
           :main 'event-handler.core}))
