Name:           event-handler
Version:        0.1.1
Release:        1%{?dist}
Summary:        event-handler executes a bash script or program when an event(e.g. DBus) occurs for system automation

License:        MIT
URL:            https://gitlab.com/vmfhrmfoaj/event-handler
Source0:        %{name}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  java-openjdk
Requires:       java >= 1:11


%description
event-handler executes a bash script or program when an event(e.g. DBus) occurs for system automation


%prep
%setup -q
which lein >/dev/null 2>&1
if [ $? -ne 0 ]; then
   wget -O ~/.local/bin/lein 'https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein'
   chmod +x ~/.local/bin/lein
fi


%build
make uberjar
sed -i 's|@BIN@|%{_bindir}/%{name}|g'                   event-handler.service
sed -i 's|@SYSCONF@|%{_sysconfdir}/sysconfig/%{name}|g' event-handler.service
sed -i 's|@CONFIG@|%{_sysconfdir}/%{name}/conf.toml|g'  event-handler.sysconfig


%install
mkdir -p %{buildroot}%{_libdir}/%{name}
mkdir -p %{buildroot}%{_sysconfdir}/%{name}
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig
mkdir -p %{buildroot}%{_localstatedir}/cache/event-handler
mkdir -p %{buildroot}%{_userunitdir}
%__install -m 644 target/%{name}                    %{buildroot}%{_bindir}/%{name}
%__install -m 644 dev-resources/config.example.toml %{buildroot}%{_sysconfdir}/%{name}/example.conf.toml
%__install -m 644 event-handler.service             %{buildroot}%{_userunitdir}/%{name}.service
%__install -m 644 event-handler.sysconfig           %{buildroot}%{_sysconfdir}/sysconfig/%{name}


%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}
%{_sysconfdir}/%{name}/example.conf.toml
%{_userunitdir}/event-handler.service
%config(noreplace) %{_sysconfdir}/sysconfig/event-handler


%changelog
* Wed Nov 23 2022 Jinseop Kim <iam@jinseop.kim> - 0.1.1a.1-1
- Release 0.1.1a.1
