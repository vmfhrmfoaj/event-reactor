(ns event-handler.core-gen
  (:require [clojure.spec.extension :as ext]
            [clojure.spec.gen.alpha :as gen]
            [event-handler.core-spec :as target-spec]))


(ext/attach-gen! ::target-spec/simple (constantly (gen/elements (mapv str "simple"))))


(comment
  (require '[clojure.test.check.generators])
  (require '[clojure.test.check.rose-tree :as rose])
  (import 'clojure.test.check.generators.Generator)
  (import 'java.util.UUID)

  ;; NOTE
  ;;  How to create custom generator:
  (gen/sample
   (Generator.
    (fn [_rng _size]
      (rose/make-rose (UUID/randomUUID) [])))))
