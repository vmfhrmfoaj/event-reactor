(ns clojure.spec.alpha.extension
  (:require [clojure.spec.alpha :as-alias spec]))

(set! *warn-on-reflection* true)


(defmacro with-explain
  "Add an explain string to the spec.

  Example:
   (`spec/def` ::string?
     (with-explain
       string?
       \"It should be string\"))"
  [x explain & explains]
  (let [form (macroexpand-1 x)]
    (cond
      (and (instance? clojure.lang.Cons form) (= `spec/map-spec-impl (first form)))
      (let [[func spec] form]
        (list func
              (update spec :pred-forms
                      (fn [pred-forms]
                        (let [[_ forms] pred-forms]
                          (->> forms
                               (mapv #(if (nil? %1)
                                        %2
                                        (list `with-explain %2 %1))
                                     (concat [explain] explains (repeat nil)))
                               (list 'quote)))))))
      (and (instance? clojure.lang.Cons form) (= `spec/rep+impl (first form)))
      (let [[func pred-form pred] form]
        (list func (list 'quote (list `with-explain pred-form explain)) pred))
      :else form)))


(defn explain-str
  "Extract an explain string that added by `with-explain` from the problem of explain data.

  Example:
   (explain-str (first (::spec/problems (`spec/explain-data` ::your-spec))))"
  [{:keys [pred]}]
  (when (sequential? pred)
    (let [[x _ explain] pred]
      (when (and (= `with-explain x) (string? explain))
        explain))))
