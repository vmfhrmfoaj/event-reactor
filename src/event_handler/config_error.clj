(ns event-handler.config-error
  (:require [clj-toml.core :as-alias toml]
            [clojure.spec.alpha.extension :as spec-ext]
            [clojure.string :as str]))

(set! *warn-on-reflection* true)


(defn report
  "Convert a problem-map to a map that contains an error message and the position of a problem.
  To find the position of a problem, this function take a TOML metadata.

  Example:
   (report {:log {::toml/start 0 ::toml/end 5} ...}, {:path [...], :in [...]})"
  [md {:keys [path in] :as problem}]
  (let [{::toml/keys [start end]}
        (->> (iterate drop-last in)
             (take-while seq)
             (reduce #(when-let [md (get-in md %2)]
                        (reduced (cond-> md
                                   (not (and (::toml/start md) (::toml/end md)))
                                   (get 0)))) ; try to find first item if array table is empty
                     nil))]
    {:id path
     :location {:start start :end end}
     :msg (spec-ext/explain-str problem)
     :problem problem}))


(defn err-msg-with-ctx
  "Like as compiler error messages, decorate an error message with its context.

  Example:
   (err-msg-with-ctx \"...config...\" 2 4 \"error message\")"
  ([conf-str start end err-msg]
   (err-msg-with-ctx conf-str start end err-msg 5))
  ([conf-str start end err-msg num-of-ctx-lines]
   (let [conf-len (count conf-str)
         start (if (< start conf-len) start (dec conf-len))
         end   (if (and (< start end) (<= end conf-len)) end (inc start))
         num-of-ctx (max 0 (dec num-of-ctx-lines))]
     (->> conf-str
          (str/split-lines)
          (map-indexed #(vector (inc %1) (count %2) %2))
          (reduce (fn [[total ctx-lines] [line-num len line]]
                    (let [prefix (str line-num ": ")
                          line   (str prefix line)
                          total+len (+ total len)]
                      (if (<= total start total+len)
                        (reduced
                         (let [indent (apply str (repeat (+ (- start total) (count prefix)) " "))
                               num-of-mark (min (- end start) (- total+len start))
                               mark (apply str indent (repeat num-of-mark "^"))
                               msg (->> (str/split-lines err-msg)
                                        (map #(str indent %))
                                        (str/join "\n"))]
                           [0 (concat (reverse ctx-lines) [line mark msg])]))
                        [(inc total+len) (conj (take num-of-ctx ctx-lines) line)]))) ; using `inc` to count newline
                  [0 ()])
          (second)
          (str/join "\n")))))


(defn summary
  "Make reports more human readable.

  Example:
   (summary \"...config...\" [{:location {:start 0 :end 5} :msg \"...\"}, ...])"
  [conf-str reports]
  (->> reports
       (filter :msg)
       (group-by :location)
       (sort-by #(:start (key %)))
       (mapcat (fn [[{:keys [start end]} reports]]
                 (let [msgs (map :msg reports)]
                   (if (and start end)
                     (->> (cond->> msgs
                            (>= (count reports) 2)
                            (map #(str "- " %)))
                          (str/join "\n")
                          (err-msg-with-ctx conf-str start end)
                          (str "\n") ; insert newline at above of a context string
                          (vector))
                     msgs))))
       (str/join "\n")
       (str/trim)))
