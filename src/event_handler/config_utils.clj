(ns event-handler.config-utils
  (:require [clojure.string :as str]))

(set! *warn-on-reflection* true)


(defn resolve-special-vars
  "Resolve special variables in 'exec' vector of handler.

  Example:
   (resolve-special-vars [\"%h/.local/xxx\", \"arg1\"])"
  [exec-vec]
  (update exec-vec 0 #(str/replace % #"^%h/" (str (System/getProperty "user.home") "/"))))
