(ns event-handler.core
  (:gen-class)
  (:require [clojure.core.async :as async :refer [go-loop <!]]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.tools.cli :as cli]
            [event-handler.config :as config]
            [event-handler.error :as err]
            [event-handler.handler :as handler]
            [event-handler.trigger :as trigger]
            [taoensso.timbre :as log]
            [taoensso.encore :as log.enc])
  (:import java.util.TimeZone))

(set! *warn-on-reflection* true)


(def ^:private options
  [["-f" "--file FILE" "Config file"
    :default (str (System/getProperty "user.home") "/.config/event-handler/default.toml")
    :validate [#(.exists (io/as-file %)) "File doesn't exist"]]

   ["-d" "--verbose LEVEL" "Verbosity level"
    :parse-fn keyword
    :validate [#{:debug :info :warn :error} "Verbosity should be one of them 'debug', 'info', 'warn' and 'error'."]]

   ["-t" "--test" "Test config file and exit"]

   [nil "--no-log-prefix" "Print logs without timestamp and hostname for Journald"]

   ["-h" "--help" "Display this message and exit"]])


(def ^:private exit (promise))


(defn- short-log-output-fn
  "Log output function for Journald."
  ([base-output-opts data]
   (let [data (cond-> data
                (seq base-output-opts)
                (assoc :output-opts
                       (conj base-output-opts            ; Opts from partial
                             (get data :output-opts))))] ; Opts from data override
     (short-log-output-fn data)))
  ([data]
   (let [{:keys [level ?err ?ns-str ?file ?line output-opts]} data]
     (str
      (str/upper-case (name level))  " "
      "[" (or ?ns-str ?file "?") ":" (or ?line "?") "] - "

      (when-let [msg-fn (get output-opts :msg-fn log/default-output-msg-fn)]
        (msg-fn data))

      (when-let [err ?err]
        (when-let [ef (get output-opts :error-fn log/default-output-error-fn)]
          (when-not   (get output-opts :no-stacktrace?) ; Back compatibility
            (log.enc/catching
             (str log.enc/system-newline (ef data)) _
             (str log.enc/system-newline "[TIMBRE WARNING]: `error-fn` failed, falling back to `pr-str`:"
                  log.enc/system-newline
                  (log.enc/catching (pr-str err) _ "<pr-str failed>"))))))))))


(defn- clean-up
  [trigger-conns receiver]
  (trigger/disconnect-all-sources!)
  (doseq [trigger-ch trigger-conns]
    (trigger/stop trigger-ch))
  (handler/stop receiver)
  (shutdown-agents)
  (deliver exit :done))


(defn- start-app
  [{:keys [handlers triggers] :as _conf}]
  (let [err-ch (async/chan (async/dropping-buffer 10))
        {handler-ch :ch, :as receiver} (handler/start handlers)
        trigger-conns (reduce #(try
                                 (conj %1 (trigger/start %2 err-ch handler-ch))
                                 (catch Exception e
                                   (let [err-msg (str "unable to start trigger '" (:name %2) "'")]
                                     (if (:quit-on-error? %2)
                                       (do
                                         (log/error err-msg e)
                                         (log/info "exit with 255 due to" (str "'" (:name %2) "'") "can't start")
                                         (System/exit 255))
                                       (log/warn err-msg e)))))
                              [] triggers)]
    (.addShutdownHook (Runtime/getRuntime) (Thread. #(clean-up trigger-conns receiver)))
    (go-loop []
      (when-let [e (<! err-ch)]
        (when-let [{:keys [type trigger]} (ex-data e)]
          (when (and (= err/unable-reocvery type) (:quit-on-error? trigger))
            (log/info "exit with 255 due to" (str "'" (:name trigger) "'") "can't recovery")
            (System/exit 255)))
        (recur)))
    @exit))


(defn -main [& args]
  (try
    (let [{opt :options help-msg :summary} (cli/parse-opts args options)]
      (cond
        (:help opt) (println help-msg)
        (:test opt) (config/load-conf-file (:file opt))
        :else
        (let [{:keys [log] :as conf} (config/load-conf-file (:file opt))
              log-lv (or (:verbose opt) (:level log))
              log-file (:file log)]
          (log/merge-config! (cond-> {:min-level log-lv
                                      :timestamp-opts {:timezone (TimeZone/getDefault)}}
                               (not= "/dev/stdout" log-file) (assoc-in [:appenders :spit] (log/spit-appender {:fname log-file}))
                               (:no-log-prefix opt)          (assoc :output-fn short-log-output-fn)))

          (start-app conf)))
      ;; NOTE
      ;;  if you don't call `shutdown-agents`, program will waiting terminate agent for a while.
      (shutdown-agents))
    (catch Exception e
      (binding [*out* *err*]
        (println (ex-message e)))
      (System/exit 255))))
