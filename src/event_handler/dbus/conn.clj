(ns event-handler.dbus.conn
  (:require [clojure.java.io :as io]
            [event-handler.error :as err]
            [taoensso.timbre :as log])
  (:import (java.io EOFException IOException)
           (java.nio.channels AsynchronousCloseException ClosedByInterruptException ClosedChannelException)
           java.util.ServiceLoader
           org.freedesktop.dbus.connections.BusAddress
           org.freedesktop.dbus.connections.config.TransportConfig
           org.freedesktop.dbus.connections.transports.AbstractTransport
           org.freedesktop.dbus.spi.transport.ITransportProvider
           org.freedesktop.dbus.utils.AddressBuilder))

(set! *warn-on-reflection* true)


(def ^:private provider
  (->> (ServiceLoader/load ITransportProvider)
       (map #(vector (.getSupportedBusType ^ITransportProvider %) %))
       (into {})))


(defn sys-bus-addr
  "Get an `DBusAddress` instance of the system bus." []
  (BusAddress/of (AddressBuilder/getSystemConnection)))


(defn sess-bus-addr
  "Get an `DBusAddress` instance of the session bus." []
  (BusAddress/of (AddressBuilder/getSessionConnection nil)))


(defn custom-bus-addr
  "Returns `DBusAddress` instance.

  Example:
   (bus-addr \"/your/custom/bus\")"
  [unix-sock-path]
  (let [f (io/as-file unix-sock-path)]
    (cond
      (not (.exists f)) (do (log/error (str "'" unix-sock-path "' is not exists")) nil)
      (.isDirectory f)  (do (log/error (str "'" unix-sock-path "' is directory"))  nil)
      :else (BusAddress. (str "unix:path=" unix-sock-path)))))


(defn create
  "Return a connection object.
  It is not connected yet. You need to use `connect` to connect to a given address.

  Example:
   (create (`sys-bus-addr`))     ; create a connection for the system bus
   (create (`sess-bus-addr`))    ; create a connection for the session bus
   (create (`sys-bus-addr`) 500) ; create a connection with 500 msecs timeout (if the conn type is not TCP, timeout will be ignored)"
  ([^BusAddress bus-addr]
   (create bus-addr 0))
  ([^BusAddress bus-addr timeout]
   (if-let [^ITransportProvider factory (provider (.getBusType bus-addr))]
     (.createTransport factory bus-addr (doto (TransportConfig.) (.setTimeout timeout)))
     (throw (ex-info "no provider" {:bus-address bus-addr :timeout timeout})))))


(defn connected?
  "Return true, if a connection is connected, otherwise return false.

  Example:
   (connected? (`create` (`sys-bus-addr`)))"
  [^AbstractTransport conn]
  (.isConnected conn))


(defn connect
  "Connect a connection to their target.

  Example:
   (connect (`create` (`sys-bus-addr`)))"
  [^AbstractTransport conn]
  (when (and conn (not (.isConnected conn)))
    (try
      (.connect conn)
      (catch IOException e
        (log/error "unable to connect" {:connection conn :error e})
        (.close conn)
        (throw e)))
    (when-not (.isConnected conn)
      (throw (ex-info "unable to connect for no reason" {:connection conn})))))


(defn disconnect
  "Close a connection.

  Example:
   (disconnect (`create` (`sys-bus-addr`)))"
  [^AbstractTransport conn]
  (when (and conn (.isConnected conn))
    (.close conn)
    true))


(defn recv-msg
  "Receive a message from a connection.

  Example:
   (read-msg (doto (`create` (`sys-bus-addr`)) (`connect`)))"
  [^AbstractTransport conn]
  (when (and conn (.isConnected conn))
    (try
      (loop [i 10]
        (if-let [msg (.readMessage conn)]
          msg
          (cond
            (<= i 0)
            (do
              (log/warn "DBus library returns `nil` continously, DBus connection seems to be closed")
              (disconnect conn)
              (throw (ex-info "DBus connection seems to be closed"
                              {:type err/io-error
                               :input [conn]})))
            (not (.isConnected conn))
            (let [err-msg "DBus connection was closed unexpectedly"]
              (log/warn err-msg)
              (throw (ex-info err-msg
                              {:type err/io-error
                               :input [conn]})))
            ;; in according to document, `readMessage` may returns `nil`.
            ;; limit retrying up to 10 times for just in case.
            :else
            (do
              (Thread/sleep 10)
              (recur (dec i))))))

      (catch AsynchronousCloseException _
        (log/info "closed connection" {:connection conn})
        (disconnect conn)
        nil)
      (catch ClosedByInterruptException _
        (log/info "closed connection by interrupt" {:connection conn})
        (disconnect conn)
        nil)
      (catch ClosedChannelException _
        (log/info "closed connection" {:connection conn})
        (disconnect conn)
        nil)
      (catch EOFException _
        (log/info "closed connection by eof" {:connection conn})
        (disconnect conn)
        nil)
      (catch IOException e
        (let [err-msg "unable to receive DBus messages"]
          (log/error err-msg{:connection conn :error e})
          (disconnect conn)
          (throw (ex-info err-msg
                          {:type err/io-error
                           :input [conn]}
                          e)))))))


(defn send-msg
  "Send a message to a connection.

  Example:
   (write-msg (doto (`create` (`sys-bus-addr`)) (`connect`)) #obj[...Message])"
  [^AbstractTransport conn msg]
  (when (and conn (.isConnected conn))
    (try
      (.writeMessage conn msg)

      (catch AsynchronousCloseException _
        (log/info "closed connection" {:connection conn})
        (disconnect conn)
        nil)
      (catch ClosedByInterruptException _
        (log/info "closed connection" {:connection conn})
        (disconnect conn)
        nil)
      (catch ClosedChannelException _
        (log/info "closed connection" {:connection conn})
        (disconnect conn)
        nil)
      (catch EOFException _
        (log/info "closed connection" {:connection conn})
        (disconnect conn)
        nil)
      (catch IOException e
        (let [err-msg "unable to send a DBus message" ]
          (log/error err-msg{:connection conn :error e})
          (disconnect conn)
          (throw (ex-info err-msg
                          {:type err/io-error
                           :input [conn msg]}
                          e)))))))


(comment
  (let [conn (create (sys-bus-addr))]
    (connect conn)
    (connected? conn)
    (disconnect conn)
    ))
