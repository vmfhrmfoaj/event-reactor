(ns event-handler.dbus.core
  "Minimal Clojure wrapper for DBus Java library."
  (:require [clojure.core.async :as async :refer [go go-loop <! >!]]
            [event-handler.dbus.conn :as conn]
            [event-handler.dbus.msg :as msg]
            [event-handler.error :as err]
            [taoensso.timbre :as log]))

(set! *warn-on-reflection* true)


(defn- connect*
  "Returns DBus connection.

  Examples:
   (connect* :session)
   (connect* :system)
   (connect* \"/your/custom/bus/path\")"
  [bus]
  (let [addr (condp = bus
               :system  (conn/sys-bus-addr)
               :session (conn/sess-bus-addr)
               (conn/custom-bus-addr bus))
        conn (conn/create addr)]
    (conn/connect conn)
    (let [hello-msg (msg/method-call "/org/freedesktop/DBus" "org.freedesktop.DBus" "Hello")]
      (conn/send-msg conn hello-msg)
      (conn/recv-msg conn)
      (doseq [type ["signal" "method_call" "method_return" "error"]]
        (let [param [(cond->> (str "type='" type "'") (not= :system bus) (str "eavesdrop=true,"))]
              watch-msg (msg/method-call "/org/freedesktop/DBus" "org.freedesktop.DBus" "AddMatch" param)]
          (conn/send-msg conn watch-msg)))
      (log/debug "ready to monitor DBus" (str "'" bus "'") "bus")
      conn)))


(defn disconnect
  "Close a DBus connection.

  Example:
   (close (`connect` :system))"
  [{:keys [conn ch] :as _dbus-conn}]
  (when-let [conn (and conn @conn)]
    (conn/disconnect conn))
  (when ch
    (async/close! ch)))


;; NOTE
;;  use `alter-var-root` to change `*max-retry*` or `*retry-delay-in-ms*` instead of `binding`.

(def ^:dynamic *max-retry* "Use `alter-var-root` to change the delay instead of `binding`" 3)

(def ^{:dynamic true :tag Long} *retry-delay-in-ms* 500)

(defn connect
  "Creates a DBus connection and runs go block to forward DBus messages to the channel.
  And returns a map that has ':trigger-kind', ':ch' and ':bus' keys.

  You can get DBus messages from the channel.

  Examples:
   (connect :session (`fn` [e] ...)) ; connect to the session bus
   (connect :system  (`fn` [e] ...)) ; connect to the system  bus
   (connect :system  (`fn` [e] ...) (`async/chan`)) ; you can use your own channel instead of"
  ([bus report-err]
   (let [nproc (.availableProcessors (Runtime/getRuntime))]
     (connect bus report-err (async/chan (async/sliding-buffer (* nproc 2))))))
  ([bus report-err ch]
   (log/debug "DBus connect to" bus {:channel ch})
   (let [conn  (atom (connect* bus))
         exit? (atom false)
         finished (promise)
         dbus-conn {:trigger-kind :dbus
                    :conn conn
                    :ch ch
                    :bus bus
                    :exit? exit?
                    :wait finished}]
     (go
       (try
         (loop [c @conn
                retry-cnt 0]
           (let [[msg err] (try [(conn/recv-msg c) nil] (catch Exception e [nil e]))]
             (cond
               msg
               (do
                 (>! ch msg)
                 (recur c 0))
               (nil? err) (log/info "DBus connection was closed")
               (< retry-cnt *max-retry*)
               (do
                 (log/error "an error has occurred", {:error err})
                 (log/info "retry to connect DBus after" *retry-delay-in-ms* "msec:" (str (inc retry-cnt) "/" *max-retry*))
                 (conn/disconnect c)
                 (Thread/sleep *retry-delay-in-ms*)
                 (recur (reset! conn (connect* bus)) (inc retry-cnt)))
               :else
               (let [err-msg "can't reocver DBus connection"]
                 (log/error "an error has occurred", {:error err})
                 (log/warn err-msg)
                 (report-err (ex-info err-msg {:type err/unable-reocvery :input [bus report-err ch]} err))))))

         (catch Exception e
           (log/error "an unexpected error has occurred" {:error e})))
       (log/debug "DBus trigger has been stopped")
       (disconnect dbus-conn)
       (reset! exit? true)
       (deliver finished dbus-conn))
     dbus-conn)))


(defn wait
  "Waits until closing DBus connection.

  Example:
   (wait (`connect` :system))"
  ([{:keys [wait] :as _dbus-conn}]
   (when wait
     (deref wait)))
  ([{:keys [wait] :as _dbus-conn} timeout]
   (when wait
     (deref wait timeout nil))))


(comment
  (let [err-ch (async/chan (async/dropping-buffer 10))
        {:keys [ch] :as sys-bus} (connect :system err-ch)
        check-hdr (every-pred (msg/msg-type-check-fn :signal)
                              (msg/msg-path-check-fn "/org/freedesktop/login1/session/**")
                              (msg/msg-interface-check-fn "org.freedesktop.DBus.Properties")
                              (msg/msg-member-check-fn "PropertiesChanged"))
        get-vars (msg/vars-extraction-fn [{:type :str,  :val "org.freedesktop.login1.Session"}
                                       {:type :dict, :val [{:type :bool, :name "LockedHint", :val "%{any}", :var "is-lock"}] :pos "%{continuous}"}])]
    (go-loop []
      (when-let [msg (<! ch)]
        (println msg)
        (when-let [var-tbl (and (check-hdr msg)
                                (get-vars msg))]
          (println var-tbl))
        (recur)))
    (Thread/sleep (* 5 1000))
    (disconnect sys-bus)))
