(ns event-handler.dbus.internal.utils
  (:require [event-handler.error :as err])
  (:import java.util.regex.Pattern))

(set! *warn-on-reflection* true)


(defn glob->regex
  "Convert a glob pattern string to a regular expression.
  I additionally support '**' pattern. This pattern indicates all characters except the newline.

  Examples:
   (glob->regex \"/a/b/*\")       ;=> #\"^/a/b/[^/]*$\"
   (glob->regex \"/a/b/**\")      ;=> #\"^/a/b/.*$\"
   (glob->regex \"/a/b/?\")       ;=> #\"^/a/b/[^/]?$\"
   (glob->regex \"/a/b/[abc]\")   ;=> #\"^/a/b/[abc]$\"
   (glob->regex \"/a/b/[a-c]\")   ;=> #\"^/a/b/[a-c]$\"
   (glob->regex \"/a/b/{a,b,c}\") ;=> #\"^/a/b/(a|b|c)$\"
   (glob->regex \"/a/b/\\*\")      ;=> #\"^/a/b/\\*$\""
  [input]
  (loop [[c & cs] input
         last-char nil
         regex "^"
         escaped? false
         in-bracket? false
         in-curly-bracket? false]
    (cond
      (nil? c)
      (if-not (or escaped? in-bracket? in-curly-bracket?)
        (re-pattern (str regex "$"))
        (throw (ex-info (str "invalid glob pattern: "
                             (cond
                               escaped?          "ends with escape character"
                               in-bracket?       "ends without closing bracket"
                               in-curly-bracket? "ends without closing curly bracket"))
                        {:type err/illegal-args :input [input]})))
      escaped? (recur cs c (str regex (Pattern/quote (str c))) false in-bracket? in-curly-bracket?)
      (= \\ c) (recur cs c regex true in-bracket? in-curly-bracket?)
      in-bracket?
      (condp = c
        \[ (throw (ex-info "invalid glob pattern: there is nested bracket" {:type err/illegal-args :input [input]}))
        \] (recur cs c (str regex "]") false false false)
        (recur cs c (str regex c)   false true false))
      in-curly-bracket?
      (condp = c
        \{ (throw (ex-info "invalid glob pattern: there is nested curly bracket" {:type err/illegal-args :input [input]}))
        \} (recur cs c (str regex ")") false false false)
        \, (recur cs c (str regex "|") false false true)
        \space
        (recur cs c (cond-> regex
                      (not= last-char \,)
                      (str " "))
               false false true)

        (recur cs c (str regex c) false false true))
      (= \[ c) (recur cs c (str regex "[")     false true  false)
      (= \{ c) (recur cs c (str regex "(")     false false true)
      (= \? c) (recur cs c (str regex "[^/]?") false false false)
      (= \* c)
      (if (= \* (first cs))
        (recur (next cs) c (str regex ".*")    false false false)
        (recur       cs  c (str regex "[^/]*") false false false))
      :else (recur cs c (str regex c) false in-bracket? in-curly-bracket?))))


(defn sig->idxes-lst
  "Returns indexes of signatures that match to a specification.

  Examples:
   (sig->idxes-lst {:sig \"i\"}                  [\"i\" \"b\" \"i\"]) ;=> [[0] [2]]
   (sig->idxes-lst {:sig \"i\" :next {:sig \"b\"}} [\"i\" \"b\" \"i\"]) ;=> [[0 1]]"
  [head-spec sigs]
  (let [reset-state #(assoc % :spec head-spec :temp [])]
    (->> sigs
         (mapv vector (range))
         (reduce (fn [{:keys [spec] :as acc} [i s :as input]]
                   (if-not (= (:sig spec) s)
                     (cond-> (reset-state acc)
                       (not= spec head-spec)
                       (recur input)) ; retry this step with the first spec if a current spec is not first.
                     (let [next (:next spec)
                           temp (conj (:temp acc) i)]
                       (if-not next
                         (update (reset-state acc) :matches conj temp)
                         (-> acc
                             (assoc :spec next)
                             (assoc :temp temp))))))
                 {:spec head-spec
                  :matches []
                  :temp []})
         (:matches))))


(comment
  [(re-find (glob->regex "/a/b/*")  "/a/b/")
   (re-find (glob->regex "/a/b/*")  "/a/b/c")
   (re-find (glob->regex "/a/b/**") "/a/b/c/d")])

(comment
  [(sig->idxes-lst {:sig "i"}                                   ["i" "ax" "i" "i" "s" "s" "i" "s" "i" "s"])
   (sig->idxes-lst {:sig "i" :next {:sig "s"}}                  ["i" "ax" "i" "i" "s" "s" "i" "s" "i" "s"])
   (sig->idxes-lst {:sig "i" :next {:sig "s" :next {:sig "b"}}} ["i" "ax" "i" "s" "i" "s" "b" "s" "i" "s"])])
