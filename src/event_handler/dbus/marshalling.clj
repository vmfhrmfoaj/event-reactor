(ns event-handler.dbus.marshalling
  #_(:import org.freedesktop.dbus.interfaces.DBusSerializable))

(set! *warn-on-reflection* true)


(declare sig)

(defn seq-sig
  "Return DBus signature for Clojure types implements Sequential.

  Examples:
   (seq-sig [1 2 3])   ;=> \"x\"
   (seq-sig [1 1.5 2]) ;=> \"v\""
  [xs]
  (when (seq xs)
    (reduce #(if (= %1 (sig %2))
               %1
               (reduced "v"))
            (sig (first xs))
            (rest xs))))


(defn type-sig
  "Return a DBus signature of the type.

  Examples:
   (type-sig java.lang.Boolean)     ;=> \"b\"
   (type-sig (type (int-array []))) ;=> \"ai\""
  [^Class typ]
  (cond
    (= Boolean typ) "b"
    (= Byte    typ) "y"
    (= Short   typ) "n"
    (= Integer typ) "i"
    (= Long    typ) "x"
    (= Float   typ) "d"
    (= Double  typ) "d"
    (= String  typ) "s"
    (.isArray typ)
    (let [type-name (.getTypeName typ)
          sig (condp = type-name
                "boolean[]" (type-sig Boolean)
                "byte[]"    (type-sig Byte)
                "short[]"   (type-sig Short)
                "int[]"     (type-sig Integer)
                "long[]"    (type-sig Long)
                "float[]"   (type-sig Float)
                "double[]"  (type-sig Float)
                (type-sig (Class/forName (subs type-name 0 (- (count type-name) 2)))))]
      (when sig
        (str "a" sig)))))


(defn sig
  "Return DBus signature(s).

  Examples:
   (sig 0)         ;=> \"x\"
   (sig 0 \"1\" 3.0) ;=> \"xsd\"
   (sig [0 \"1\"])   ;=> \"av\" ; variant array
   (sig [0 1])     ;=> \"ax\" ; long(64 bit integer) array"
  ([x]
   (cond
     (keyword? x) "s"
     (sequential? x) (str "a" (seq-sig x))
     ;; NOTE
     ;;  I think, in DBus coventionally use 's{sv}' signature for dictionary type.
     (map? x) "a{sv}"
     #_(let [k (seq-sig (keys x))
             v (seq-sig (vals x))]
         (when (and k v)
           (str "a{" k v "}")))
     (set? x)
     (when-let [s (seq-sig (seq x))]
       (str "a" s))
     :else (type-sig (class x))))
  ([x & xs]
   (->> (cons x xs)
        (map sig)
        (apply str))))


(defn dbus-vals
  "Convert Clojure data structure(e.g. map) to DBus value."
  [xs]
  ;; TODO
  ;;  convert map, vector, list, lazy-seq and etc to `DBusSerializable`
  #_(reify DBusSerializable
      (serialize [this]
        ))
  xs
  )


(defn flag
  "Return a byte that indicate DBus flags.

  Examples:
   (flag) ;= (flag {})
   (flag :auto-start? true)
   (flag {:no-reply?         true
          :auto-start?       true
          :interactive-auth? true})"
  [& {:keys [no-reply? auto-start? interactive-auth?] :as _opt}]
  (cond-> (byte 0)
    no-reply?         (bit-or 0x01)
    auto-start?       (bit-or 0x02)
    interactive-auth? (bit-or 0x04)))


(comment
  (sig "org.freedesktop.login1.Session" {:LockedHint true})
  )
