(ns event-handler.handler
  (:require [clojure.core.async :as async :refer [go <! >!]]
            [clojure.string :as str]
            [event-handler.error :as err]
            [event-handler.time :as time]
            [taoensso.timbre :as log])
  (:import (java.lang ProcessBuilder Process)
           java.util.function.Consumer
           java.util.Scanner))

(set! *warn-on-reflection* true)


(defn resolve-vars
  "Returns an argument that resolved variables in it.

  Example:
   (resolve-vars {\"key\" \"value\"} \"key is %{key}\")"
  [var-tbl arg]
  (str/replace arg #"(?<=^|[^%])%\{([-._0-9A-Za-z]+)\}" #(str (var-tbl (second %1)))))


(defn handler-fn
  "Return a function that take a variable map and return a promise object that store the exit value of a program.

  Example:
   ((handler-fn {:name \"test\" :exec [\"/bin/bash\" \"-c\" \"...\"]}) nil)"
  [{:keys [name exec] :as handler}]
  (let [[pgm & args] exec]
    (when-not pgm
      (throw (ex-info "invalid handler" {:type err/illegal-args :input [handler]})))
    (fn [var-tbl]
      (let [var-tbl (or var-tbl {})
            args (reduce (fn [args arg]
                           ;; To resolve '%{...}' expression, I used regular expression look-behind.
                           ;; see, https://www.regular-expressions.info/lookaround.html
                           (conj args (resolve-vars var-tbl arg)))
                         [pgm]
                         args)
            args (into-array String args)
            pb (doto (ProcessBuilder. ^"[Ljava.lang.String;" args)
                 (.redirectErrorStream true))]
        (log/info "run" (str "'" name "'") "handler:" exec)
        (let [proc (.start pb)
              output (future
                       (let [scanner (-> proc (.getInputStream) (Scanner.))]
                         (loop [s ""]
                           (cond
                             (.hasNextLine scanner) (recur (str s "\n" (.nextLine scanner)))
                             (> (count s) 1)        (subs s 1)
                             :else                  ""))))
              ch (async/promise-chan)]
          (-> proc
              (.onExit)
              (.thenAccept (reify Consumer
                             (accept [_ proc]
                               ;; TODO
                               ;;  - terminate a process with a timeout for just in case the program is waiting user input
                               (let [exit-val (.exitValue ^Process proc)]
                                 (log/info (str name "(" (.pid ^Process proc) ")") "is exit with" exit-val
                                           (if (= 0 exit-val)
                                             ""
                                             @output))
                                 (async/go (async/>! ch exit-val)))))))
          [ch proc])))))


(defn run-handler
  "Run a handler asynchronously.

  Examples:
   (run-handler {:func #(println %) :retry-after nil})
   (run-handler {:func #(println %) :retry-after nil} {\"key\" \"val\"})"
  ([handler]
   (run-handler handler nil))
  ([{:keys [func retry-after] :as handler} var-tbl]
   (when-not func
     (throw (ex-info "no func" {:type err/illegal-args :input [handler var-tbl]})))
   (let [[res-ch proc] (func var-tbl)
         timeout-ch (async/timeout 10000)] ; make it configurable
     (go
       (async/alt!
         timeout-ch
         (let [name (str (handler :name) "(" (.pid ^Process proc) ")")]
           (log/warn name "is not finished in" 10 "seconds")
           (log/info "kill" name "process")
           (.destroy ^Process proc))

         res-ch
         ([exit-code]
          (when (and (< exit-code 0) retry-after)
            (log/info "retry" (handler :name) "after" retry-after "seconds.")
            (<! (async/timeout retry-after))
            (run-handler (dissoc handler :retry-after) var-tbl)))))
     res-ch)))


(defn- process-all-delayed-handlers
  ([events]
   (process-all-delayed-handlers events false))
  ([events wait?]
   (when-not (empty? events)
     (log/info "launch all delayed handlers," (if wait? "synchronously" "asynchronously"))
     (doseq [{:keys [handler var-tbl]} events]
       (cond-> (run-handler (dissoc handler :retry-after) var-tbl)
         wait? (async/<!!))))))


(defn- add-delayed-handler
  [handlers new-job]
  ;; TODO
  ;;  - use the heap instead of vector
  (log/debug "add delayed event:" new-job)
  (sort-by :time #(time/before? %1 %2) (conj handlers new-job)))


(defn start-delay-executor
  "Run go-routine that processes delayed handlers, and returns a channel for adding delayed handlers.

  Example:
   (start-timer)
   (start-timer (`async/chan`))"
  ([]
   (start-delay-executor (-> (Runtime/getRuntime) (.availableProcessors) (async/buffer) (async/chan))))
  ([new-handler-ch]
   (go
     (log/info "handler timer is starting")
     ;; TODO
     ;;  - use the heap instead of vector
     (loop [delayed-handlers []]
       (let [{:keys [handler var-tbl time]} (first delayed-handlers)
             timeout (volatile! 0)]
         (cond
           (nil? handler)
           (if-let [new-handler (<! new-handler-ch)]
             (recur (add-delayed-handler delayed-handlers new-handler))
             (process-all-delayed-handlers delayed-handlers true))
           (or (nil? time) (not (pos-int? (vreset! timeout (-> (time/now) (time/between time) (time/second :milli))))))
           (do
             (run-handler handler var-tbl)
             (recur (next delayed-handlers)))
           :else
           (let [timer-ch (async/timeout @timeout)]
             (async/alt!
               timer-ch
               (do
                 (run-handler handler var-tbl)
                 (recur (next delayed-handlers)))
               new-handler-ch
               ([new-handler]
                (if new-handler
                  (recur (add-delayed-handler delayed-handlers new-handler))
                  (process-all-delayed-handlers delayed-handlers true))))))))
     (log/info "handler timer was stopped"))
   new-handler-ch))


(defn start
  "Run a go-block that run handlers when receiving events.
  And return a channel to use to receive the events.

  Examples:
   (start [{:name \"test\" :exec [\"/bin/bash\" \"-c\" \"...\"]}])
   (start [{:name \"test\" :exec [\"/bin/bash\" \"-c\" \"...\"]}] (`async/chan`))
   (start [{:name \"test\" :func #function[...]}]) ; you can run a function instead of a program of the 'exec'"
  ([handlers]
   (start handlers
          (-> (Runtime/getRuntime)
              (.availableProcessors)
              (async/buffer)
              (async/chan))))
  ([handlers handler-ch]
   (let [delay-exec-ch (start-delay-executor)
         find-handler (->> handlers
                           (map #(cond-> %
                                   (not (fn? (:func %)))
                                   (assoc :func (handler-fn %))))
                           (reduce #(assoc %1 (:name %2) %2) {}))]
     (go
       (log/info "start handler")
       (loop []
         (when-let [{:keys [handler-name var-tbl delay] :as ev} (<! handler-ch)]
           (log/info "received an event:" ev)
           (let [handler (find-handler handler-name)
                 delay (or delay (:delay handler))]
             (cond
               (nil? handler)                  (log/warn "no" handler-name "handler")
               (or (nil? delay) (zero? delay)) (run-handler handler var-tbl)
               (pos-int? delay)
               (let [fire-time (time/plus (time/now) delay :milli)]
                 (log/info handler-name "handler will execute after" delay "ms")
                 (>! delay-exec-ch {:handler handler :var-tbl var-tbl :time fire-time}))))
           (recur)))
       (log/info "handler was stopped")
       (async/close! delay-exec-ch)))
   {:handlers handlers
    :ch handler-ch}))


(defn stop
  "Stop the handler receiver.

  Example:
   (stop-handler {:handlers [...], :ch #obj[...Channel]})"
  [{:keys [ch]}]
  (when ch
    (async/close! ch)))


(defn event
  "Create a new event.

  Examples:
   (event \"test\" {\"key\" \"value\"})
   (event \"test\" {\"key\" \"value\"} 1000)"
  ([handler-name var-tbl]
   (event handler-name var-tbl nil))
  ([handler-name var-tbl delay]
   {:handler-name handler-name
    :var-tbl      var-tbl
    :delay        delay}))
