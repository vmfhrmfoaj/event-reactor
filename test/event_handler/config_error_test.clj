(ns event-handler.config-error-test
  (:require [clj-toml.core :as-alias toml]
            [clojure.spec.alpha.extension :as spec-ext]
            [clojure.string :as str]
            [clojure.test :refer :all]
            [event-handler.config-error :as target]))


(deftest examples
  (testing "report"
    (let [toml-md {:log {:file {::toml/start 0 ::toml/end 5}}}
          problem-map {:path [:log :file]
                       :in   [:log :file]
                       :pred `(spec-ext/with-explain nil "err-msg")}
          {:keys [location msg]} (target/report toml-md problem-map)]
      (is (= 0 (:start location)))
      (is (= 5 (:end   location)))
      (is (string? msg))))

  (testing "err-msg-with-ctx"
    (let [conf-str (str/join "\n"
                             ["01234567"
                              "abcdefgh"
                              "ABCDEFGH"
                              "01234567"
                              "abcdefgh"
                              "ABCDEFGH"
                              "01234567"
                              "abcdefgh"
                              "ABCDEFGH"])
          msg "error message"]
      (is (= (str/join "\n"
                       [(str "2: abcdefgh")
                        (str "3: ABCDEFGH")
                        (str "4: 01234567")
                        (str "5: abcdefgh")
                        (str "6: ABCDEFGH")
                        (str "7: 01234567")
                        (str "      ^^")
                        (str "      " msg)])
             (target/err-msg-with-ctx conf-str 57 59 msg)))))

  (testing "summary"
    (let [conf-str (str/join "\n" ["01234567" "abcdefgh"])
          problems [{:location {:start  2 :end  4} :msg "err msg #1"}
                    {:location {:start  2 :end  4} :msg "err msg #2"}
                    {:location {:start 14 :end 17} :msg "err msg #3"}]]
      (is (= (str/join "\n"
                       ["1: 01234567"
                        "     ^^"
                        "     - err msg #1"
                        "     - err msg #2"
                        ""
                        "1: 01234567"
                        "2: abcdefgh"
                        "        ^^^"
                        "        err msg #3"])
             (target/summary conf-str problems))))))


(deftest exceptional-cases
  (testing "the range(start and end) is out of the context"
    (let [conf-str "01234567"
          msg "error message"]
      (is (= (str/join "\n"
                       [(str "1: 01234567")
                        (str "          ^")
                        (str "          " msg)])
             (target/err-msg-with-ctx conf-str 102 104 msg)))))

  (testing "reversed range"
    (let [conf-str "01234567"
          msg "error message"]
      (is (= (str/join "\n"
                       [(str "1: 01234567")
                        (str "     ^")
                        (str "     " msg)])
             (target/err-msg-with-ctx conf-str 2 0 msg))))))
