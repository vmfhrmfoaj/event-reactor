(ns event-handler.dbus.core-test
  (:require [clojure.core.async :as async]
            [clojure.core.async.extension :as async-ext]
            [clojure.test :refer :all]
            [dev.test :as test :refer [with-hard-redefs]]
            [event-handler.dbus.core :as target]
            [event-handler.dbus.conn :as conn]
            [spy.core :as spy]
            [spy.extension :as spy-ext]))

(use-fixtures :once test/without-trivial-log)


(deftest examples
  (testing "connect*"
    ;; TODO
    )

  (testing "disconnect"
    (let [disconn (spy/stub nil)
          close   (spy/stub nil)
          dbus-conn {:conn (atom :dbus-conn) :ch :async-chan}]
      (with-redefs [conn/disconnect disconn
                    async/close! close]
        (target/disconnect dbus-conn)
        (is (spy/called-once-with? disconn :dbus-conn))
        (is (spy/called-once-with? close   :async-chan)))))

  (testing "connect"
    (let [;; NOTE
          ;;  I think `proxy` create an instance with the default method (that throwing exception) for abstract methods.
          ;;   after creating the instance, replace the default method to custom method.
          ;;  `AbstractTransport` class calls abstract method in the constructor. So, an exception is occurred, always.
          o (Object.)  ; (proxy [AbstractTransport] [(conn/sess-bus-addr)] ...)
          conn (constantly o)
          msgs [:msg-1 :msg-2 :msg-3]
          recv-msg (let [i (volatile! -1)] (fn [& _] (get msgs (vswap! i inc))))
          discon (constantly nil)
          report-fn (spy/stub nil)
          ch (async/chan (async/buffer 5))]
      (with-hard-redefs [target/connect* conn
                         conn/recv-msg   recv-msg
                         conn/disconnect discon]
        (let [dbus-conn (target/connect :session report-fn ch)]
          (target/wait dbus-conn)

          (is (= ch       (:ch    dbus-conn)))
          (is (= o (deref (:conn  dbus-conn))))
          (is (deref      (:exit? dbus-conn)))
          (is (async-ext/closed? ch))
          (is (= :msg-1 (async-ext/<!! ch 1000)))
          (is (= :msg-2 (async-ext/<!! ch 1000)))
          (is (= :msg-3 (async-ext/<!! ch 1000)))
          (is (not (spy/called? report-fn))))))))


(deftest recovery
  (testing "if an error occurred while receiving DBus messages, retry to recover up to 3 times"
    (let [o (Object.)
          conn (spy/stub o)
          msgs [:error! :error! :msg-1 :msg-2 :error! :msg-3 :error! :error! :error! :error! :msg-4]
          ;;   ^      ^       ^                     ^              ^       ^       ^               ^
          ;;          discon  discon                disconn        discon  discon  discon          discon
          ;;   conn   conn    conn                  conn           conn    conn    conn
          recv-msg (let [i (volatile! -1)]
                     (fn [& _]
                       (let [rv (get msgs (vswap! i inc))]
                         (when (= :error! rv)
                           (throw (Exception. "ERROR!")))
                         rv)))
          discon (spy/stub nil)
          bus :session
          report-fn (spy/stub nil)
          ch (async/chan (async/buffer 5))]
      (with-hard-redefs [target/*retry-delay-in-ms* 0
                         target/connect* conn
                         conn/recv-msg   recv-msg
                         conn/disconnect discon]
        (let [dbus-conn (target/connect bus report-fn ch)]
          (target/wait dbus-conn)

          (is (= ch       (:ch    dbus-conn)))
          (is (= o (deref (:conn  dbus-conn))))
          (is (deref      (:exit? dbus-conn)))
          (is (async-ext/closed? ch))
          (is (= :msg-1 (async-ext/<!! ch 1000)))
          (is (= :msg-2 (async-ext/<!! ch 1000)))
          (is (= :msg-3 (async-ext/<!! ch 1000)))
          (is (nil?     (async-ext/<!! ch 1000)))
          (is (spy/called-n-times-with? conn 7 #(= [bus] %)))
          (is (spy/called-n-times? discon 7)) ; recovery (6) + clean-up (1)
          (is (spy/called? report-fn)))))))
