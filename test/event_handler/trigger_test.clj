(ns event-handler.trigger-test
  (:require [clojure.core.async :as async]
            [clojure.core.async.extension :as async-ext]
            [clojure.test :refer :all]
            [dev.test :as test]
            [event-handler.handler :as handler]
            [event-handler.dbus.core :as dbus]
            [event-handler.trigger :as target]
            [event-handler.time :as time]
            [spy.core :as spy]
            [spy.extension :as spy-ext]))

(use-fixtures :once test/without-trivial-log)


(deftest examples
  (testing "dbus-trigger-fn"
    (is (fn? (target/dbus-source-fn {:bus :system
                                     :type :signal
                                     :path "/org/freedesktop/login1/session/**"
                                     :interface "org.freedesktop.DBus.Properties"
                                     :member "PropertiesChanged"
                                     :args [{:type :str
                                             :val "org.freedesktop.login1.Session"}
                                            {:type :dict
                                             :val [{:type :bool
                                                    :name "LockedHint"
                                                    :val "%{any}"
                                                    :var "is-lock"}]
                                             :pos "%{continuous}"}]}))))

  (testing "trigger-fn"
    (is (= (handler/event "change-cpu-freq-policy" {"is-lock" true})
           (let [handler-ch (async/chan)
                 tgr-fn (target/trigger-fn {:name "Screen (un)lock"
                                            :interval 1000
                                            :conj :or
                                            :time-slice 100
                                            :seq-check? false
                                            :sources [{:type :dbus
                                                       :data {:bus :system
                                                              :type :signal
                                                              :path "/org/freedesktop/login1/session/**"
                                                              :interface "org.freedesktop.DBus.Properties"
                                                              :member "PropertiesChanged"
                                                              :args [{:type :str
                                                                      :val "org.freedesktop.login1.Session"}
                                                                     {:type :dict
                                                                      :val [{:type :bool
                                                                             :name "LockedHint"
                                                                             :val "%{any}"
                                                                             :var "is-lock"}]
                                                                      :pos "%{continuous}"}]}}]
                                            :handlers [{:name "change-cpu-freq-policy"}]}
                                           handler-ch)]
             (tgr-fn 0 {"is-lock" true})
             (let [res (async-ext/<!! handler-ch 1000)]
               (async/close! handler-ch)
               res)))))

  (testing "disconnect-source!"
    (let [dbus-disconn (spy/stub true)
          data :dbus-conn-data
          multi (async/chan) ; mocking multi
          src {:data data :multi multi}
          src-type [:dbus :system]]
      (reset! @#'target/source {src-type (assoc src :count 1)})
      (is (some? (get @@#'target/source src-type)))

      (with-redefs [dbus/disconnect dbus-disconn]
        (target/disconnect-source! src-type {:data data :multi multi}))

      (is (spy/called-once-with? dbus-disconn data))
      (is (nil? (get @@#'target/source src-type)) "`disconnect-source!` should clean up source's status")
      (is (async-ext/closed? multi) "`disconnect-source!` should close branch channel")))

  (testing "connect-source!"
    (let [src-ch (async/chan 1)
          dup-ch (async/chan 1)
          report-fn identity
          conn (spy/stub {:ch src-ch})]
      (reset! @#'target/source nil)
      (is (nil? @@#'target/source))

      (with-redefs [dbus/connect conn]
        (is (= dup-ch (target/connect-source! [:dbus :session] report-fn dup-ch))))

      (is (spy/called-once-with? conn :session report-fn))
      (is (some? (get @@#'target/source [:dbus :session])))

      (async/>!! src-ch :abc)
      (is (= :abc (async-ext/<!! dup-ch 1000))
          "the cloned channel should be connected to the source channel")

      (async/close! src-ch)
      (is (async-ext/closed? dup-ch)
          "the cloned channel should be closed when the source is closed")))

  (testing "start"
    (let [handler-ch (async/chan)
          trigger-ch (async/chan)
          src-ch (async/chan)
          err-ch (async/chan)
          src-data {:bus :system
                    :type :signal
                    :path "/org/freedesktop/login1/session/**"
                    :interface "org.freedesktop.DBus.Properties"
                    :member "PropertiesChanged"
                    :args [{:type :str
                            :val "org.freedesktop.login1.Session"}
                           {:type :dict
                            :val [{:type :bool
                                   :name "LockedHint"
                                   :val "%{any}"
                                   :var "is-lock"}]
                            :pos "%{continuous}"}]}
          trigger-conf {:name "Screen (un)lock"
                        :interval 1000
                        :conj :or
                        :time-slice 100
                        :seq-check? false
                        :quit-on-error? true
                        :sources [{:type :dbus
                                   :data src-data}]
                        :handlers [{:name "change-cpu-freq-policy"}]}
          conn-src (spy/stub src-ch)
          start-dbus-src (spy/stub true)]
      (with-redefs [target/connect-source! conn-src
                    target/start-dbus-src start-dbus-src]
        (is (= {:trigger trigger-conf
                :source-chs [src-ch]
                :ch trigger-ch}
               (target/start trigger-conf err-ch handler-ch trigger-ch))))

      (is (spy/called-once-with? conn-src [:dbus :system] spy-ext/fn))
      (is (spy/called-once-with? start-dbus-src 0 src-data src-ch trigger-ch))

      (async/close! trigger-ch)
      (async/close! src-ch)))

  (testing "stop"
    (let [trigger-ch (async/chan)
          src-ch (async/chan)]
      (target/stop {:source-chs [src-ch] :ch trigger-ch})

      (is (async-ext/closed? trigger-ch))
      (is (async-ext/closed? src-ch)))))


(deftest test-trigger-conditions
  (testing "interval"
    (let [handler-ch (async/chan)
          interval 1000
          tgr-fn (target/trigger-fn {:name "Screen (un)lock"
                                     :interval interval
                                     :conj :or
                                     :time-slice 100
                                     :seq-check? false
                                     :sources [{:type :dbus
                                                :data {:bus :system
                                                       :type :signal
                                                       :path "/org/freedesktop/login1/session/**"
                                                       :interface "org.freedesktop.DBus.Properties"
                                                       :member "PropertiesChanged"
                                                       :args [{:type :str
                                                               :val "org.freedesktop.login1.Session"}
                                                              {:type :dict
                                                               :val [{:type :bool
                                                                      :name "LockedHint"
                                                                      :val "%{any}"
                                                                      :var "is-lock"}]
                                                               :pos "%{continuous}"}]}}]
                                     :handlers [{:name "change-cpu-freq-policy"}]}
                                    handler-ch)
          now (time/now)]
      (with-redefs [time/now (constantly now)]
        (tgr-fn 0 {"is-lock" true}))
      (is (= (handler/event "change-cpu-freq-policy" {"is-lock" true})
             (async-ext/<!! handler-ch 1000)))
      (with-redefs [time/now (constantly (time/plus now (dec interval) :milli))]
        (tgr-fn 0 {"is-lock" false}))
      (with-redefs [time/now (constantly (time/plus now interval :milli))]
        (tgr-fn 0 {"is-lock" true}))
      (is (= (handler/event "change-cpu-freq-policy" {"is-lock" true})
             (async-ext/<!! handler-ch 1000)))

      (async/close! handler-ch)))

  (testing "source conjunction"
    (let [handler-ch (async/chan)
          tgr-fn (target/trigger-fn {:name "Screen (un)lock"
                                     :interval 1000
                                     :conj :or
                                     :time-slice 100
                                     :seq-check? false
                                     :sources [{:type :dbus
                                                :data {:bus :system
                                                       :type :signal
                                                       :path "/org/freedesktop/login1/session/**"
                                                       :interface "org.freedesktop.DBus.Properties"
                                                       :member "PropertiesChanged"
                                                       :args [{:type :str
                                                               :val "org.freedesktop.login1.Session"}
                                                              {:type :dict
                                                               :val [{:type :bool
                                                                      :name "LockedHint"
                                                                      :val "%{any}"
                                                                      :var "is-lock"}]
                                                               :pos "%{continuous}"}]}}
                                               {:type :dbus
                                                :data {:bus :system
                                                       :type :signal
                                                       :path "/org/freedesktop/login2/session/**"
                                                       :interface "org.freedesktop.DBus.Properties"
                                                       :member "PropertiesChanged"
                                                       :args [{:type :str
                                                               :val "org.freedesktop.login2.Session"}
                                                              {:type :dict
                                                               :val [{:type :bool
                                                                      :name "LockedHint"
                                                                      :val "%{any}"
                                                                      :var "is-lock"}]
                                                               :pos "%{continuous}"}]}}]
                                     :handlers [{:name "change-cpu-freq-policy"}]}
                                    handler-ch)
          now (time/now)]
      (with-redefs [time/now (constantly now)]
        (tgr-fn 0 {"is-lock" true}))
      (is (= (handler/event "change-cpu-freq-policy" {"is-lock" true})
             (async-ext/<!! handler-ch 1000)))
      (with-redefs [time/now (constantly (time/plus now 1 :second))]
        (tgr-fn 1 {"is-lock" false}))
      (is (= (handler/event "change-cpu-freq-policy" {"is-lock" false})
             (async-ext/<!! handler-ch 1000)))

      (async/close! handler-ch))

    (let [handler-ch (async/chan)
          tgr-fn (target/trigger-fn {:name "Screen (un)lock"
                                     :interval 1000
                                     :conj :and
                                     :time-slice 100
                                     :seq-check? false
                                     :sources [{:type :dbus
                                                :data {:bus :system
                                                       :type :signal
                                                       :path "/org/freedesktop/login1/session/**"
                                                       :interface "org.freedesktop.DBus.Properties"
                                                       :member "PropertiesChanged"
                                                       :args [{:type :str
                                                               :val "org.freedesktop.login1.Session"}
                                                              {:type :dict
                                                               :val [{:type :bool
                                                                      :name "LockedHint"
                                                                      :val "%{any}"
                                                                      :var "is-lock"}]
                                                               :pos "%{continuous}"}]}}
                                               {:type :dbus
                                                :data {:bus :system
                                                       :type :signal
                                                       :path "/org/freedesktop/login2/session/**"
                                                       :interface "org.freedesktop.DBus.Properties"
                                                       :member "PropertiesChanged"
                                                       :args [{:type :str
                                                               :val "org.freedesktop.login2.Session"}
                                                              {:type :dict
                                                               :val [{:type :bool
                                                                      :name "LockedHint"
                                                                      :val "%{any}"
                                                                      :var "is-lock-2"}]
                                                               :pos "%{continuous}"}]}}]
                                     :handlers [{:name "change-cpu-freq-policy"}]}
                                    handler-ch)
          now (time/now)]
      (with-redefs [time/now (constantly now)]
        (tgr-fn 0 {"is-lock"   true})
        (tgr-fn 1 {"is-lock-2" true}))
      (is (= (handler/event "change-cpu-freq-policy"
                            {"is-lock"   true
                             "is-lock-2" true})
             (async-ext/<!! handler-ch 1000))
          "if 'conjunction' is 'and', `trigger-fn` should check all sources")

      (async/close! handler-ch)))

  (testing "time window(slice)"
    (let [handler-ch (async/chan 1)
          time-slice 100
          tgr-fn (target/trigger-fn {:name "Screen (un)lock"
                                     :interval 1000
                                     :conj :and
                                     :time-slice time-slice
                                     :seq-check? false
                                     :sources [{:type :dbus
                                                :data {:bus :system
                                                       :type :signal
                                                       :path "/org/freedesktop/login1/session/**"
                                                       :interface "org.freedesktop.DBus.Properties"
                                                       :member "PropertiesChanged"
                                                       :args [{:type :str
                                                               :val "org.freedesktop.login1.Session"}
                                                              {:type :dict
                                                               :val [{:type :bool
                                                                      :name "LockedHint"
                                                                      :val "%{any}"
                                                                      :var "is-lock"}]
                                                               :pos "%{continuous}"}]}}
                                               {:type :dbus
                                                :data {:bus :system
                                                       :type :signal
                                                       :path "/org/freedesktop/login2/session/**"
                                                       :interface "org.freedesktop.DBus.Properties"
                                                       :member "PropertiesChanged"
                                                       :args [{:type :str
                                                               :val "org.freedesktop.login2.Session"}
                                                              {:type :dict
                                                               :val [{:type :bool
                                                                      :name "LockedHint"
                                                                      :val "%{any}"
                                                                      :var "is-lock-2"}]
                                                               :pos "%{continuous}"}]}}]
                                     :handlers [{:name "change-cpu-freq-policy"}]}
                                    handler-ch)
          now (time/now)]
      (with-redefs [time/now (constantly now)]
        (tgr-fn 0 {"is-lock"   true}))
      (with-redefs [time/now (constantly (time/plus now 99 :milli))]
        (tgr-fn 1 {"is-lock-2" true}))
      (is (= (handler/event "change-cpu-freq-policy"
                            {"is-lock"   true
                             "is-lock-2" true})
             (async-ext/<!! handler-ch 1000)))

      (with-redefs [time/now (constantly (time/plus now 2 :second))]
        (tgr-fn 0 {"is-lock"   false}))
      (with-redefs [time/now (constantly (time/plus now (+ 2000 (inc time-slice)) :milli))]
        (tgr-fn 1 {"is-lock-2" false}))

      (async/close! handler-ch) ; to prevent waiting for the timeout
      (is (nil? (async/<!! handler-ch)))))

  (testing "sequence check"
    (let [handler-ch (async/chan 1)
          interval 1000
          tgr-fn (target/trigger-fn {:name "Screen (un)lock"
                                     :interval interval
                                     :conj :and
                                     :time-slice 100
                                     :seq-check? true
                                     :sources [{:type :dbus
                                                :data {:bus :system
                                                       :type :signal
                                                       :path "/org/freedesktop/login1/session/**"
                                                       :interface "org.freedesktop.DBus.Properties"
                                                       :member "PropertiesChanged"
                                                       :args [{:type :str
                                                               :val "org.freedesktop.login1.Session"}
                                                              {:type :dict
                                                               :val [{:type :bool
                                                                      :name "LockedHint"
                                                                      :val "%{any}"
                                                                      :var "is-lock"}]
                                                               :pos "%{continuous}"}]}}
                                               {:type :dbus
                                                :data {:bus :system
                                                       :type :signal
                                                       :path "/org/freedesktop/login2/session/**"
                                                       :interface "org.freedesktop.DBus.Properties"
                                                       :member "PropertiesChanged"
                                                       :args [{:type :str
                                                               :val "org.freedesktop.login2.Session"}
                                                              {:type :dict
                                                               :val [{:type :bool
                                                                      :name "LockedHint"
                                                                      :val "%{any}"
                                                                      :var "is-lock-2"}]
                                                               :pos "%{continuous}"}]}}]
                                     :handlers [{:name "change-cpu-freq-policy"}]}
                                    handler-ch)
          now (time/now)]
      (with-redefs [time/now (constantly now)]
        (tgr-fn 0 {"is-lock"   true}))
      (with-redefs [time/now (constantly (.plusMillis now 10))]
        (tgr-fn 1 {"is-lock-2" true}))
      (is (= (handler/event "change-cpu-freq-policy"
                            {"is-lock"   true
                             "is-lock-2" true})
             (async-ext/<!! handler-ch 1000)))

      (with-redefs [time/now (constantly (.plusMillis now (+ interval 10)))]
        (tgr-fn 1 {"is-lock-2" false}))
      (with-redefs [time/now (constantly (.plusMillis now (+ interval 20)))]
        (tgr-fn 0 {"is-lock"   false}))

      (async/close! handler-ch) ; to prevent waiting for the timeout
      (is (nil? (async-ext/<!! handler-ch))))))
